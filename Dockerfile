ARG GRAFANA_VERSION="main-ubuntu"

FROM grafana/grafana-enterprise:${GRAFANA_VERSION}

USER root

RUN apt -y update && \
    apt -y upgrade

USER grafana