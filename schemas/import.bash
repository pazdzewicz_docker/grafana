#!/bin/bash
PSQL="$(which psql)"
FIND="$(which find)"

SCHEMA_FOLDER="/schemas/"
SCHEMA_FILE="${1}"
SCHEMA_PATH="${SCHEMA_FOLDER}/${SCHEMA_FILE}"

CLEAR_FILE="clear.sql"
CLEAR_PATH="${SCHEMA_FOLDER}/${CLEAR_FILE}"

if ! [ -f ${SCHEMA_PATH} ]
then
  echo "File does not exist"
  echo "Possible Files:"
  ${FIND} "${SCHEMA_FOLDER}" -iname "*export*.sql" -printf "- %f\n"
  exit 1
fi

echo "CLEAR EXISTING DATABASE"
${PSQL} --host="localhost" --username="${POSTGRES_USER}" --dbname="postgres" -f "${CLEAR_PATH}"

echo "IMPORT SCHEMA FILE ${SCHEMA_FILE}"
${PSQL} --host="localhost" --username="${POSTGRES_USER}" --dbname="${POSTGRES_DB}" -f "${SCHEMA_PATH}"
echo "DONE"