# Grafana Docker

Ready-to-run Grafana Docker Umgebung.

## Setup
### Wichtig

[Traefik](https://gitlab.com/pazdzewicz_docker/traefik) wird empfohlen, ansonsten kommentiert das Traefik Network und die Labels für normalen Port-Zugriff. Dies funktioniert auch während Traefik aktiv ist!

### 1. Environment erstellen:

Wir kopieren uns das `.env.template` nach `.env` um dann die Konfigurationsvariablen zu vergeben.

```
cp .env.template .env

nano .env
```

Nun die Variablen anhand der Dokumentation und Kommentare anpassen.

### 2. Docker

Jetzt können wir unsere Docker Umgebung starten. Bitte beachte das der Traefik Server gestartet sein muss, sonst erhältst du eine Netzwerk Fehlermeldung von Docker.

```
docker-compose pull
docker-compose up -d
```

### 3. Fertig

Nun können wir unser Grafana über die Web-UI nutzen, dafür rufen wir in unserem Webbrowser `https://${WEB_HOST}` oder `http://<IP>:3000` und melden uns in den in der `.env` vergebenen Nutzerdaten für den `admin` Nutzer