#!/bin/bash
DOCKER_COMPOSE="$(which docker-compose)"
CONTAINER="directus-db"
SCRIPT="/schemas/export.bash"

if [ -z "${DOCKER_COMPOSE}" ]
then
  DOCKER_COMPOSE="$(which docker) compose"
fi

"${DOCKER_COMPOSE}" exec "${CONTAINER}" "${SCRIPT}"