#!/bin/bash
SCRIPT_PATH="$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"
DOCKER_COMPOSE_PATH="${SCRIPT_PATH}/../docker-compose.yml"
ENV_PATH="${SCRIPT_PATH}/../.env"
DOCKER_COMPOSE="$(which docker-compose)"
DOCKER_COMPOSE_PATH="${SCRIPT_PATH}/../docker-compose.yml"
COMMAND="${@}"

# Check docker-compose standalone or plugin
if [ -z "${DOCKER_COMPOSE}" ]
then
  DOCKER_COMPOSE="$(which docker) compose"
fi

source "${ENV_PATH}"
${DOCKER_COMPOSE} -f "${DOCKER_COMPOSE_PATH}" ${COMMAND}
